### 블로그 구성 정보

1. 프로젝트 디렉토리
```
├── manage.py
├── myblog
│   ├── forms.py
│   ├── settings.py
│   ├── static
│   │   │   ├── css
│   │   │   ├── base.css
│   │   │   ├── delete.css
│   │   │   ├── detail.css
│   │   │   ├── edit.css
│   │   │   ├── less
│   │   │   │   ├── base.less
│   │   │   │   ├── delete.css
│   │   │   │   ├── detail.less
│   │   │   │   ├── edit.less
│   │   │   │   ├── list.less
│   │   │   │   └── login.less
│   │   │   ├── list.css
│   │   │   └── login.css
│   │   ├── js
│   │   │   ├── coffee
│   │   │   │   ├── detail.coffee
│   │   │   │   └── edit.coffee
│   │   │   ├── detail.js
│   │   │   └── edit.js
│   │   ├── templates
│   │   ├── base.html
│   │   └── login_form.html
│   ├── tests
│   │   ├── __init__.py
│   │   ├── login.py
│   │   ├── __pycache__
│   │   │   ├── __init__.cpython-35.pyc
│   │   │   ├── login.cpython-35.pyc
│   │   │   └── user.cpython-35.pyc
│   │   └── user.py
│   ├── urls.py
│   ├── views.py
│   └── wsgi.py
├── post
│   ├── admin.py
│   ├── apps.py
│   ├── forms.py
│   ├── models.py
│   ├── templates
│   │   └── post
│   │       ├── post_confirm_delete.html
│   │       ├── post_create_form.html
│   │       ├── post_detail.html
│   │       ├── post_list.html
│   │       └── post_update_form.html
│   ├── tests.py
│   ├── urls.py
│   └── views.py
├── README.md
└── record.md
```
---

2. 주요 정보
    * DATABASE **postgresql**
    * static 디렉토리 **/myblog/static**
    * 현재 한글 초성 변환이 적용되있다. `/myblog/static/js/detail.js`
    * textarea에 Mark Down 기호에 관한 highlight 기능을 추가 `/myblog/static/js/edit.js`
