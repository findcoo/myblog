## 블로그 개발일지

+ postgresql 설정
	+ postgresql.conf에서 인코딩, 타임존 등을 사전에 설정한다
		e.g. 
        ```django setting
        client_encoding='UTF8'
		default_transaction_isolation='read committed' //데이터베이스와 모델 계층의 독립성에 대한 설정
		timezone='ROK'
        ```

	+ pycopg2 라이브러리 의존성을 확보한다.
		### e.g. 
		```ubuntu
        apt-get build-dep python3-pycopg2
		pip install pycopg2
        ```

	
	+ settings.py에서 데이터베이스의 기본정보와 테스트 데이터베이스 정보를 등록한다.
	+ 타임존 설정을 일치 시켜준다.
	+ django.contrib.postgres를 추가하여 postgresql관련 장고 모듈 의존성을 확보한다.

+ 장고 인증 계층
	+ `python manage.py migrate` 를 실행하여 기본 테이블들을 생성한다.
	+ `python manage.py createsuperuser --username=<user> --email=<user email>`를 실행하여 슈퍼유저를 생성한다.
	+ 인증 계층은 계정 생성, 인증, 비밀번호 암호화, 유효성 검사등을 제공하며 User 모델을 확장하여 사용 할 수 있다.
	+ 모델의 접근 권한을 객체로 만들어 사용자에게 부여 할 수 있다. 단 기본 내장 객체들에 관한 권한 부여는 기본적으로 금지사항이다.

+ 인증 trick
    + Changing passsword
    ``` django
    from django.contrib.auth.models import User

    u = User.objects.get(username='john')
    u.set_password('new password')
    u.save()
    ```
    + Authenticating users
    ```django
    from django.contrib.auth import authenticate
    

    user = authenticate(username='john', password='secret')
    if user is not None:
        if user.is_activate:
            


+ 테스트 케이스 작성
	+ test.py 에 작성하며 TestCase 클래스를 불러와 사용한다.
	+ assert[비교대상] 형식의 카멜케이스로 된 테스트 실행 메소드가 존재한다.
	+ def setUp 함수를 정의하여 테스트에 필요한 상황을 설정한다.
	+ `python manage.py test <appname | None>` 형식으로 시행한다.
	+ 왠만하면 테스트 케이스에서 먼저 구현후 모듈을 적용할 것을 권한다.

