from django.test import TestCase, RequestFactory
from django.test import Client

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

class LoginViewTests(TestCase):
    def setUp(self):
        self.user = Client()
        self.factory = RequestFactory()
        user = User.objects.create_user('test', 'test@test.com', 'test1234')


    def test_login_page(self):
        response = self.user.post(reverse('login'), data={'username_or_email': 'test', 'password': 'test1234'})
        self.assertEqual(response.status_code, 200)

    def test_email_login_page(self):
        response = self.user.post(reverse('login'), data={'username_or_email': 'test@test.com', 'password': 'test1234'})
        self.assertEqual(response.status_code, 200)

        

