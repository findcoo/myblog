from django.test import TestCase
from django.contrib.auth.models import User, Permission
from django.contrib.auth import authenticate, login
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django.http import request

class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create_user("Tester", "test@test.com", "test1234") 

        content_type = ContentType.objects.get_for_model(User)
        Permission.objects.create(codename="create_user",
                                name="user manager",
                                content_type=content_type)


    def test_create_user(self):
        user = User.objects.get(username="Tester")
        self.assertEquals(user.username, "Tester")

    def test_authenticating(self):
        user1 = authenticate(username="Tester", password="test1234")
        user2 = authenticate(username="Tester", password="1111") 

        self.assertIsNone(user2)
        self.assertIsNotNone(user1)

    def test_authenticate_with_email(self):
        user1 = authenticate(email="test@test.com", password="test1234")

        self.assertIsNone(user1)

    def test_permission(self):
        user = get_object_or_404(User, username="Tester")
        self.assertEquals(user.username, "Tester")

        perm_check = user.has_perm('create_user')
        self.assertFalse(perm_check)

        permission = Permission.objects.get(codename="create_user")
        self.assertEquals(permission.codename, "create_user")

        user.user_permissions.add(permission)
        user = get_object_or_404(User, username="Tester")
        perm_check = user.has_perm('create_user')
        self.assertFalse(perm_check)
