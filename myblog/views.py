from django.http import HttpResponseRedirect, JsonResponse

from django.contrib.auth import login, authenticate

from django.views import generic
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from .forms import LoginForm 

class LoginView(FormView):
    template_name = "login_form.html"
    form_class = LoginForm
    success_url = reverse_lazy('post:index')
    
    def form_valid(self, form):
        uid=form.cleaned_data['username']
        ups=form.cleaned_data['password']

        user = authenticate(username=uid, password=ups)
        if user is None:
            return JsonResponse({"asd": "adsd"})
        login(self.request, user)

        return super(LoginView, self).form_valid(form)
   
