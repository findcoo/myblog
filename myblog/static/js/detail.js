(function() {
  $(function() {
    return $(window).load(function() {
      var choseong, choseong_btn, clicked, context, text_backup;
      context = $("dd:eq(1)");
      text_backup = context.text();
      context.html(text_backup);
      choseong = function() {
        var c, c_to_code, choseong_arr, choseong_scope, code_to_index, hangul_uni_start, i, j, len, result, text;
        choseong_arr = ['ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'o', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'];
        hangul_uni_start = 44032;
        choseong_scope = 588;
        text = context.text();
        result = "";
        for (i = j = 0, len = text.length; j < len; i = ++j) {
          c = text[i];
          if (c.match(/[가-힣]/i)) {
            c_to_code = c.charCodeAt(0);
            code_to_index = parseInt((c_to_code - hangul_uni_start) / choseong_scope);
            result += choseong_arr[code_to_index];
          }
        }
        context.html(text.replace(/[가-깋]/, "ㄱ"));
        return result;
      };
      choseong_btn = $("<button type='button' class='btn btn-primary btn-xs active' id='choseong'>초성</button>");
      $(choseong_btn).insertAfter("blockquote");
      clicked = false;
      return $("#choseong").click(function() {
        var r;
        if (clicked === false) {
          r = choseong();
          context.append("<br>" + r);
          return clicked = true;
        } else {
          context.html(text_backup);
          return clicked = false;
        }
      });
    });
  });

}).call(this);
