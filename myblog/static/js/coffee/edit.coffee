$ ->
    area = document.getElementById "textarea"
    real_area = document.getElementById "id_context"

    md_regex =
        "Header": /#(?!<\/font>)/g
        "Em": /\*(?!<\/font>)/g
        "List": /\d\.(?!<\/font>)/g
        "Link": /\[[^\]]+\]:(?!<\/font>)/g
        "Image": /\!\[[^\]]+\]:(?!<\/font>)/g
        "Code": /`(?!<\/font>)/g
        "Horizon": /~{3}(?!<\/font>)|\*{3}(?!<\/font>)|\-{3}(?!<\/font>)/g

    md_color =
        "Header": "<font color=red>#</font>"
        "Em": "<font color=red>*</font>"
        "List": (data) ->
                    "<font color=orange>"+data+"</font>"
        "Link": (data) ->
                    "<font color=green>"+data+"</font>"
        "Image": (data) ->
                    "<font color=green>"+data+"</font>"
        "Code": "<font color=red>`</font>"
        "Horizon": (data) ->
                    "<font color=blue>"+data+"</font>"
    set_dot = ->
        caret = window.getSelection()
        chk_dot = document.createElement "span"
        chk_dot.id = "dotted"
        caret.getRangeAt(0).insertNode chk_dot
        caret

    to_dot = (caret) ->
        range = document.createRange()
        span_el = document.getElementById "dotted"

        range.selectNode span_el
        caret.removeAllRanges()
        caret.addRange range
        range.deleteContents()

        

    syntax_on = ->
        $.each md_regex, (key, value) ->
            if key in ["List","Image","Link", "Horizon"]
                matched = $(area).html().match value
                $.each matched, (index, value) ->
                    data = $(area)
                            .html()
                            .replace md_regex[key], md_color[key] value
                    $(area).html data
            else
                data = $(area)
                        .html()
                        .replace md_regex[key], md_color[key]
                $(area).html data
    
    if $("h1").text() is "Update"
        $("#textarea").html $("#id_context").val()

    $(area).keypress (e) ->
        if e.which in [13, 32]
            dot = set_dot()
            syntax_on()
            to_dot dot
            
           
    $(area).blur ->
        target = $(area).html()
        value = target.replace /<(span)>|<[font color=].*?>|<\/font>/g, ""

        $(real_area).val value
