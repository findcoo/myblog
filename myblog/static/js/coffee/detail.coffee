$ ->
    $(window).load ->
        
        context = $("dd:eq(1)")
        text_backup = context.text()
        context.html text_backup
        
        choseong = ->
            choseong_arr = ['ㄱ','ㄲ','ㄴ','ㄷ','ㄸ'
                            ,'ㄹ','ㅁ','ㅂ','ㅃ','ㅅ','ㅆ',
                            'o','ㅈ','ㅉ','ㅊ','ㅋ','ㅌ','ㅍ',
                            'ㅎ']
            hangul_uni_start = 44032
            choseong_scope = 588
            text = context.text()
            result = ""
            for c, i in text
                if c.match /[가-힣]/i
                    c_to_code = c.charCodeAt 0
                    code_to_index = parseInt (c_to_code-hangul_uni_start)/choseong_scope
                    result += choseong_arr[code_to_index]
            context.html text.replace /[가-깋]/, "ㄱ"
            result

            
        choseong_btn = $("<button type='button' class='btn btn-primary btn-xs active' id='choseong'>초성</button>")
        $(choseong_btn).insertAfter "blockquote"

        clicked = false
        $("#choseong").click ->
            if clicked is false
                r = choseong()
                context.append "<br>"+r
                clicked = true
            else
                context.html text_backup
                clicked = false
            
