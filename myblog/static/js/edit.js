(function() {
  $(function() {
    var area, md_color, md_regex, real_area, set_dot, syntax_on, to_dot;
    area = document.getElementById("textarea");
    real_area = document.getElementById("id_context");
    md_regex = {
      "Header": /#(?!<\/font>)/g,
      "Em": /\*(?!<\/font>)/g,
      "List": /\d\.(?!<\/font>)/g,
      "Link": /\[[^\]]+\]:(?!<\/font>)/g,
      "Image": /\!\[[^\]]+\]:(?!<\/font>)/g,
      "Code": /`(?!<\/font>)/g,
      "Horizon": /~{3}(?!<\/font>)|\*{3}(?!<\/font>)|\-{3}(?!<\/font>)/g
    };
    md_color = {
      "Header": "<font color=red>#</font>",
      "Em": "<font color=red>*</font>",
      "List": function(data) {
        return "<font color=orange>" + data + "</font>";
      },
      "Link": function(data) {
        return "<font color=green>" + data + "</font>";
      },
      "Image": function(data) {
        return "<font color=green>" + data + "</font>";
      },
      "Code": "<font color=red>`</font>",
      "Horizon": function(data) {
        return "<font color=blue>" + data + "</font>";
      }
    };
    set_dot = function() {
      var caret, chk_dot;
      caret = window.getSelection();
      chk_dot = document.createElement("span");
      chk_dot.id = "dotted";
      caret.getRangeAt(0).insertNode(chk_dot);
      return caret;
    };
    to_dot = function(caret) {
      var range, span_el;
      range = document.createRange();
      span_el = document.getElementById("dotted");
      range.selectNode(span_el);
      caret.removeAllRanges();
      caret.addRange(range);
      return range.deleteContents();
    };
    syntax_on = function() {
      return $.each(md_regex, function(key, value) {
        var data, matched;
        if (key === "List" || key === "Image" || key === "Link" || key === "Horizon") {
          matched = $(area).html().match(value);
          return $.each(matched, function(index, value) {
            var data;
            data = $(area).html().replace(md_regex[key], md_color[key](value));
            return $(area).html(data);
          });
        } else {
          data = $(area).html().replace(md_regex[key], md_color[key]);
          return $(area).html(data);
        }
      });
    };
    if ($("h1").text() === "Update") {
      $("#textarea").html($("#id_context").val());
    }
    $(area).keypress(function(e) {
      var dot, ref;
      if ((ref = e.which) === 13 || ref === 32) {
        dot = set_dot();
        syntax_on();
        return to_dot(dot);
      }
    });
    return $(area).blur(function() {
      var target, value;
      target = $(area).html();
      value = target.replace(/<(span)>|<[font color=].*?>|<\/font>/g, "");
      return $(real_area).val(value);
    });
  });

}).call(this);
