from django import forms

from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(label='ID', label_suffix='', max_length=64)
    password = forms.CharField(label='PASSWORD', label_suffix='', widget=forms.PasswordInput, max_length=64)
