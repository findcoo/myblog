from django.views.generic import ListView, DetailView
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required

from post.models import Post, Comment
from post.forms import PostCreateForm, PostUpdateForm

@method_decorator(login_required, name='dispatch')
class PostListView(ListView):
    queryset = Post.objects.order_by('-pub_date')
    template_name = 'post/post_list.html'
    
@method_decorator(login_required, name='dispatch')
class PostDetailView(DetailView):
    model = Post
    template_name = 'post/post_detail.html'

@method_decorator(login_required, name='dispatch')
class PostCreateView(CreateView):
    model = Post
    form_class = PostCreateForm
    template_name = 'post/post_create_form.html'
    success_url = reverse_lazy('post:index')

    def form_valid(self, form):
        user = self.request.user
        form.instance.author = user
        return super(PostCreateView, self).form_valid(form)
    
@method_decorator(login_required, name='dispatch')
class PostUpdateView(UpdateView):
    model = Post
    form_class = PostUpdateForm
    template_name = 'post/post_update_form.html'
    success_url = reverse_lazy('post:index')

@method_decorator(login_required, name='dispatch')
class PostDeleteView(DeleteView):
    model = Post
    template_name = 'post/post_confirm_delete.html'
    success_url = reverse_lazy('post:index')
