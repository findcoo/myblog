from django import forms
from django.forms import ModelForm
from .models import Post 

class PostCreateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Post
        fields = ['post_subject', 'post_title', 'context']
        labels = {
                'post_subject': '주제',
                'post_title': '제목',
                'context': '내용'
        }
        widgets = {'context': forms.HiddenInput()}

class PostUpdateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super().__init__(*args, **kwargs)

    class Meta:
        model = Post
        fields = ['post_subject', 'post_title', 'context']
        labels = {
                'post_subject': '주제',
                'post_title': '제목',
                'context': '내용'
        }
        widgets = {'context': forms.HiddenInput()}
