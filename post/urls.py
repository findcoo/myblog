from django.conf.urls import url

from . import views


app_name = 'post'
urlpatterns = [
        url(r'^$', views.PostListView.as_view(), name="index"),
        url(r'^(?P<pk>[0-9]+)/$', views.PostDetailView.as_view(), name="detail"),
        url(r'^create/$', views.PostCreateView.as_view(), name="create"),
        url(r'^update/(?P<pk>[0-9]+)/$', views.PostUpdateView.as_view(), name="update"),
        url(r'^delete/(?P<pk>[0-9]+)/$', views.PostDeleteView.as_view(), name="delete")
]
