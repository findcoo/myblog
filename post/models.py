from django.db import models

from django.contrib.auth.models import User

class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    post_subject = models.CharField(max_length=64)
    post_title = models.CharField(max_length=64)
    context = models.TextField()
    pub_date = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.post_title

class Comment(models.Model):
    author = models.ForeignKey(User)
    context = models.TextField()
    pub_date = models.DateField(auto_now=True)

    def __str__(self):
        return self.author
